package com.tw;

public class StringCalculator {
    public int add(String string) {
        String[] stringNumber = {};

        // split string number by \n and ,
        stringNumber = string.split("\n|,");

        try {
            if (!stringNumber[0].equals("")) {
                Integer.parseInt(stringNumber[0]);
            }
        } catch (Exception e) {
            // the first line is separator
            stringNumber = stringNumber[1].split("[" + stringNumber[0] + "]");
        }

        int result = 0;
        for (String strNum : stringNumber) {
            if (!strNum.equals("")) {
                result += Integer.parseInt(strNum);
            }
        }

        return result;
    }
}
